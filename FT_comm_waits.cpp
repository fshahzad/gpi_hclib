#include "FT_comm_waits.h"



gaspi_return_t FT_gaspi_wait( gaspi_segment_id_t gm_seg_failed_proc_list_id, 
								gaspi_queue_id_t queue_id_, 
								gaspi_timeout_t TIMEOUT_VAL_){

	gaspi_return_t retval= (gaspi_return_t) 99;
	
	gaspi_pointer_t gm_ptr_failed_proc_list_;
	if (gaspi_segment_ptr (gm_seg_failed_proc_list_id, &gm_ptr_failed_proc_list_) != GASPI_SUCCESS)
	{
		printf ("gm_seg_failed_proc_list_id failed !\n");
		exit (-1);
	}
	gaspi_rank_t numprocs_;
	gaspi_proc_num(&numprocs_);
	int *gm_ptr_failed_proc_list= new int[numprocs_];
	gm_ptr_failed_proc_list = (int * )gm_ptr_failed_proc_list_;
	
// 	gaspi_printf("gm_ptr_failed_proc_list[0] %d\n", gm_ptr_failed_proc_list[0]);
	// NOTE: Here gm_ptr_glo_health_chk_flg[0] == WORKING can not be checked. This wait is called from global health_check call, which is called by idle procs only. In case of failure, idle procs set this flag to BROKEN on every process. All process now check global_health_check and If the 'if' condition is checked here no process will enter the gaspi_wait becuase the flag is set to broken.
	while(retval!=GASPI_SUCCESS &&  retval != GASPI_ERROR && gm_ptr_failed_proc_list[0] == WORKING){
		retval = gaspi_wait(queue_id_ , TIMEOUT_VAL_);
		if(HClib_verbose>=2)		HClib_PRINT_RETVAL(retval);
	}
	return retval;
}

gaspi_return_t FT_gaspi_notify_waitsome(	gaspi_segment_id_t gm_seg_failed_proc_list_id,
											gaspi_segment_id_t seg_id_, 
											gaspi_notification_id_t not_id, 
											gaspi_number_t not_num_, 
											gaspi_notification_id_t* first_id, 
											gaspi_timeout_t TIMEOUT_VAL_)
{
	gaspi_return_t retval= (gaspi_return_t) 99;
	gaspi_pointer_t gm_ptr_failed_proc_list_;
	if (gaspi_segment_ptr (gm_seg_failed_proc_list_id, &gm_ptr_failed_proc_list_) != GASPI_SUCCESS)
	{
		printf ("gm_seg_failed_proc_list_id failed !\n");
		exit (-1);
	}
	gaspi_rank_t numprocs_;
	gaspi_proc_num(&numprocs_);
	int *gm_ptr_failed_proc_list= new int[numprocs_];
	gm_ptr_failed_proc_list = (int * )gm_ptr_failed_proc_list_;
	while(retval!=GASPI_SUCCESS && retval != GASPI_ERROR && gm_ptr_failed_proc_list[0] == WORKING)
	{
		if(HClib_verbose>=2)		gaspi_printf("gm_ptr_failed_proc_list[0] %d\n", gm_ptr_failed_proc_list[0]);
		
		retval = gaspi_notify_waitsome(seg_id_, not_id, not_num_ , first_id,  TIMEOUT_VAL_);
		if(HClib_verbose>=2)		HClib_PRINT_RETVAL(retval);
	}
	return retval;
}

gaspi_return_t FT_gaspi_allreduce( gaspi_segment_id_t gm_seg_failed_proc_list_id,
								   gaspi_pointer_t buffer_send, 
								   gaspi_pointer_t buffer_receive , 
								   gaspi_number_t num, 
								   gaspi_operation_t operation, 
								   gaspi_datatype_t datatype , 
								   gaspi_group_t group, 
								   gaspi_timeout_t TIMEOUT_VAL_){
	gaspi_return_t retval = (gaspi_return_t) 99;

	gaspi_pointer_t gm_ptr_failed_proc_list_;
	if (gaspi_segment_ptr (gm_seg_failed_proc_list_id, &gm_ptr_failed_proc_list_) != GASPI_SUCCESS)
	{
		printf ("gm_seg_failed_proc_list_id failed !\n");
		exit (-1);
	}
	
	gaspi_rank_t numprocs_;
	gaspi_proc_num(&numprocs_);
		
	int *gm_ptr_failed_proc_list= new int[numprocs_];
	gm_ptr_failed_proc_list = (int * )gm_ptr_failed_proc_list_;
	
	
// 	gaspi_printf("gm_ptr_failed_proc_list[0] %d\n", gm_ptr_failed_proc_list[0]);
	while(retval!=GASPI_SUCCESS && retval != GASPI_ERROR && gm_ptr_failed_proc_list[0] == WORKING){
		retval = gaspi_allreduce( buffer_send, buffer_receive, num, operation, datatype , group, TIMEOUT_VAL_);
		if(HClib_verbose>=2)		HClib_PRINT_RETVAL(retval);
	}
	return retval;
}

void HClib_print_retval(const char* file, const int line, const int retval){
	if(retval == GASPI_SUCCESS){
		gaspi_printf("gaspi_return is GASPI_SUCCESS(%d) in %s[%i]\n", retval, file, line );
	}
	else if(retval == GASPI_TIMEOUT){
		gaspi_printf("gaspi_return is GASPI_TIMEOUT(%d) in %s[%i]\n", retval, file, line);
	}
	else if(retval == GASPI_ERROR){
		gaspi_printf("gaspi_return is GASPI_ERROR(%d) in %s[%i]\n", retval, file, line);
	}
	else{
		gaspi_printf("gaspi_return without any valid message");
	}
	return;
}