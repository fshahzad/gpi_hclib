#ifndef __HEALTH_CHECK_H__
#define __HEALTH_CHECK_H__

#include <GASPI.h>
#include "timing.h"
#include "FT_comm_waits.h"
#include <omp.h>

static int HClib_verbose=0;


class HC{			// health checker or health manager
public:
	
	int 	* gm_ptr_status_processes;
	gaspi_rank_t * gm_ptr_failed_proc_list;
	gaspi_rank_t * gm_ptr_rescue_proc_list;
	gaspi_rank_t * gm_ptr_avoid_proc_list;
	gaspi_rank_t * gm_ptr_myrank_active_list;
	int 	* gm_ptr_queue_id_counter;
	gaspi_state_vector_t gm_ptr_health_vec;

	gaspi_segment_id_t gm_seg_status_processes_id;		// length= numprocs; checks if process is WORKING, IDLE, BROKEN, WORKFINISHED
	gaspi_segment_id_t gm_seg_failed_proc_list_id;		// length= numprocs; It contains the process-ranks of the failed processes. the zeroth element contains how many processes have died. the next elements contain their ranks.
	gaspi_segment_id_t gm_seg_rescue_proc_id;			// length = numprocs; It contains the process-ranks of the rescue processes. The zeroth element contains hwo many rescue processes are there. The next element contains their ranks.
	gaspi_segment_id_t gm_seg_avoid_list_id; 			// length = numprocs; contains the list of already failed proceses. This is needed so that we do not send them health check messages unnecessarily.
	gaspi_segment_id_t gm_seg_myrank_active_id;			// length = numprocs; Each process must know which process has which active-ranks. A process rank=10 may have replaced rank of 2 (after failue of rank-2). Each process must know this information for right communication.
	gaspi_segment_id_t gm_seg_queue_id_counter_id;		// NOTE: necesssary to change queue_id after every recovery until gaspi_queue_purge is implemented.
	gaspi_segment_id_t gm_seg_health_vec_id;			// NOTE: This is the health_vec segment that has 0, 1 status coppied from the internal health routine 
	// gaspi_segment_id_t gm_seg_health_chk_array_id=3;		// NOTE: perhaps this is not needed with ping style health checks. 
	// gaspi_segment_id_t gm_seg_CP_count_id=10;			// NOTE: not needed
	
	HC();
	~HC();
	int init();
// 	send_global_msg_to_check_state();
	
	int		stay_idle_and_check_health( gaspi_rank_t & myrank_active);
	bool	am_i_rescue_process ();
	int 	recover_comm(gaspi_group_t * COMM_MAIN, gaspi_rank_t numprocs_working, gaspi_rank_t * comm_main_ranks);
	int 	signal_all_processes(int signal );
	int 	define_sub_comm(int flag, gaspi_rank_t numprocs_COMM, gaspi_group_t * COMM, gaspi_rank_t * comm_ranks);
	int		print_health_vec();
	int 	reset_gm_ptr_failed_proc_list();
	int 	distribute_working_or_idle( int numprocs_idle);
	int 	get_status();	
	
private:
	gaspi_rank_t numprocs_idle_;
	const gaspi_rank_t numprocs_, myrank_; 
	gaspi_rank_t * idle_proc_list;
	gaspi_rank_t * previous_comm_main_rank_working_order;
	
	template <class gm_ptr_typ> int allocate_gaspi_global_mem(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gm_ptr_typ ** global_array, gaspi_group_t gaspi_group);
	int allocate_gaspi_global_mem_int(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, int ** global_array, gaspi_group_t gaspi_group);
	int allocate_gaspi_global_mem_rankt(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gaspi_rank_t ** global_array, gaspi_group_t gaspi_group);
	int allocate_gaspi_global_mem_statevectort(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gaspi_state_vector_t *state_vec, gaspi_group_t gaspi_group);

	int gaspi_printf_array(char * print_str, int * array_ptr, int num_elem);
	int gaspi_printf_array(char * print_str, gaspi_rank_t * array_ptr, int num_elem);
	int gaspi_printf_array(char * print_str, double * array_ptr, int num_elem);


	template <class init_array_typ> int init_array(init_array_typ * to_init_array, int num_elem, init_array_typ val);
	int init_array_dbl(double * to_init_array, int num_elem, double val);
	int init_array_int(int * to_init_array, int num_elem, int val);
	int init_array_gaspirankt( gaspi_rank_t * to_init_array, int num_elem, gaspi_rank_t val);

	template <class copy_array_typ> int copy_array(copy_array_typ * src, copy_array_typ * dst, int array_len);
	int copy_array_int(int * src, int * dst, int array_len);
	int copy_array_gaspirankt(gaspi_rank_t * src, gaspi_rank_t * dst, int array_len);

	int refresh_comm_main_ranks_working_order(gaspi_group_t *COMM_MAIN, gaspi_rank_t * previous_comm_main_rank_working_order, gaspi_rank_t * comm_main_ranks_ordered, gaspi_rank_t numprocs_working);
	
	
	int 	send_global_msg_to_check_state(gaspi_rank_t * idle_proc_list);	
	int 	make_failed_proc_list( gaspi_rank_t * avoid_list_old );
	int 	make_rescue_proc_list( int num_failed_procs, gaspi_rank_t *idle_proc_list, gaspi_rank_t * rescue_proc_list);
	int 	update_status_processes_array();
	int 	report_failed_processes(int num_failed_procs, gaspi_rank_t * idle_proc_list);
	int 	refresh_numprocs_working_and_idle();
	int 	update_myrank_active(gaspi_rank_t &myrank_active);
	int 	refresh_idle_proc_list(int num_failed_procs_idle, gaspi_rank_t * idle_proc_list);
	
//	int refresh_comm_main_ranks_working_order(gaspi_rank_t * previous_comm_main_rank_working_order, gaspi_rank_t * comm_main_ranks_ordered, gaspi_rank_t numprocs_working);	// TODO: this call should put the new_comm_main_ranks in the proper order . e.g. if case proc-1 has failed. and proc-3 replaces it, new_comm_main_ranks by default is 0,2,3. This call should make it 0,3,2. This would be useful to e.g redetermine the communication routines.
};


int init_myrank_active(HC *myHC);

// int 	check_comm_health (int *status_processes, gaspi_state_vector_t health_vec);

enum status{
	IDLE = 2,
	BROKEN = 1,
	WORKING = 0,
	WORKFINISHED = 9
};

void success_or_exit_HC ( const char* file, const int line, const int ec);
#define ASSERT_HC(ec) success_or_exit_HC (__FILE__, __LINE__, ec);

#endif
