#include <GASPI.h>
#include <stdio.h>
#include <stdlib.h>
#include "gpihc.h"

#define HClib_PRINT_RETVAL(ec) HClib_print_retval(__FILE__, __LINE__, ec);
void HClib_print_retval(const char * file , const int line, const int ec);

gaspi_return_t FT_gaspi_wait(
								gaspi_segment_id_t gm_seg_failed_proc_list_id,
								gaspi_queue_id_t queue_id_, 
								gaspi_timeout_t TIMEOUT_VAL_);

gaspi_return_t FT_gaspi_notify_waitsome(
								gaspi_segment_id_t gm_seg_failed_proc_list_id,
								gaspi_segment_id_t seg_id_, 
								gaspi_notification_id_t not_id, 
								gaspi_number_t not_num_, 
								gaspi_notification_id_t* first_id, 
								gaspi_timeout_t TIMEOUT_VAL_); 

gaspi_return_t FT_gaspi_allreduce( 
								gaspi_segment_id_t gm_seg_failed_proc_list_id,
								gaspi_pointer_t buffer_send, 
								gaspi_pointer_t buffer_receive , 
								gaspi_number_t num, 
								gaspi_operation_t operation, 
								gaspi_datatype_t datatype , 
								gaspi_group_t group, 
								gaspi_timeout_t TIMEOUT_VAL_);
