include ./make.defines

CXX = mpiCC
CF = mpif90
CFFLAGS += -fPIC -openmp -O3 -warn all 
CPPFLAGS +=  -fPIC -Wall -qopenmp -qopenmp-link=static -pthread -O3 -pedantic $(INCLUDES) $(DEFINES)
LIBS +=
INCLUDES +=
TARGET = libgpihc.a
LFLAGS =

OBJS = gpihc.o FT_comm_waits.o timing.o

$(TARGET): $(OBJS)
	ar rvs $(TARGET) $(OBJS)

gpihc.o: gpihc.cpp gpihc.h
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c gpihc.cpp

FT_comm_waits.o: FT_comm_waits.cpp FT_comm_waits.h
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c FT_comm_waits.cpp

timing.o:  timing.h timing.c
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c timing.c

clean:
	rm -f *.o *.a
