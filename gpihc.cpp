#include <string.h>
#include "gpihc.h"
#include <unistd.h>



gaspi_rank_t HCLIB_get_numprocs(){
	gaspi_rank_t numprocs_temp;
	gaspi_proc_num(&numprocs_temp);
	return numprocs_temp;
}
gaspi_rank_t HCLIB_get_myrank(){
	gaspi_rank_t myrank_temp;
	gaspi_proc_rank(&myrank_temp);
	return myrank_temp;
}

HC::HC(): numprocs_(HCLIB_get_numprocs()), myrank_(HCLIB_get_myrank()){
	gm_seg_queue_id_counter_id	= 2;			// NOTE: necesssary to change queue_id after every recovery until gaspi_queue_purge is implemented.
	gm_seg_myrank_active_id		= 4;			// length = numprocs; Each process must know which process has which active-ranks. A process rank=10 may have replaced rank of 2 (after failue of rank-2). Each process must know this information for right communication.
	gm_seg_rescue_proc_id 		= 5;			// length = numprocs; It contains the process-ranks of the rescue processes. The zeroth element contains hwo many rescue processes are there. The next element contains their ranks.
	gm_seg_health_vec_id		= 6;			// NOTE: This is the health_vec segment that has 0, 1 status coppied from the internal health routine 
	gm_seg_avoid_list_id 		= 7; 			// length = numprocs; contains the list of already failed proceses. This is needed so that we do not send them health check messages unnecessarily.
	gm_seg_status_processes_id	= 8;			// length= numprocs; checks if process is WORKING, IDLE, BROKEN, WORKFINISHED
	gm_seg_failed_proc_list_id	= 9;			// length= numprocs; It contains the process-ranks of the failed processes. the zeroth element contains how many processes have died. the next elements contain their ranks.
}
HC::~HC(){
	
}


int HC::init(){
	

// 	==== allocating the memories. all initializations are with 0. ====
	allocate_gaspi_global_mem_int(gm_seg_status_processes_id	, (gaspi_size_t)(numprocs_*sizeof(int)), 		&gm_ptr_status_processes, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_rankt(gm_seg_failed_proc_list_id	, (gaspi_size_t)(numprocs_*sizeof(gaspi_rank_t)), 	&gm_ptr_failed_proc_list, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_rankt(gm_seg_rescue_proc_id 		, (gaspi_size_t)(numprocs_*sizeof(gaspi_rank_t)), 	&gm_ptr_rescue_proc_list, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_rankt(gm_seg_avoid_list_id 		, (gaspi_size_t)(numprocs_*sizeof(gaspi_rank_t)), 	&gm_ptr_avoid_proc_list, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_rankt(gm_seg_myrank_active_id		, (gaspi_size_t)(numprocs_*sizeof(gaspi_rank_t)), 	&gm_ptr_myrank_active_list, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_int(gm_seg_queue_id_counter_id		, (gaspi_size_t)(numprocs_*sizeof(int))	, 			&gm_ptr_queue_id_counter, 	GASPI_GROUP_ALL);
	allocate_gaspi_global_mem_statevectort(gm_seg_health_vec_id		, (gaspi_size_t)(numprocs_*sizeof(gaspi_state_vector_t)), &gm_ptr_health_vec, 	GASPI_GROUP_ALL);	
	
	idle_proc_list	= new gaspi_rank_t[numprocs_];
	init_myrank_active(this);			// to init the myrank_active list on all processes
	return 0;
}


int HC::distribute_working_or_idle( int numprocs_idle){
	numprocs_idle_ = numprocs_idle;
	init_array_int(gm_ptr_status_processes, numprocs_, WORKING);
	init_array_gaspirankt(idle_proc_list, numprocs_idle_, 0);
	int numprocs_working = numprocs_ - numprocs_idle_;
	
	for(int i=numprocs_working,j=0; i < numprocs_; ++i,++j){
		gm_ptr_status_processes[i] = IDLE; 			// putting initial few processes to IDLE
		idle_proc_list[j]=i;
	}
	return 0;
}


int HC::stay_idle_and_check_health(gaspi_rank_t & myrank_active){
	while(1){
		usleep(100000);
		if(gm_ptr_failed_proc_list[0]!=BROKEN){			// NO FAILURE REPORTED 
			if(myrank_==numprocs_-1){
				int num_failures_detected = 0;
// 					gaspi_printf("checking the global health state now\n");
				num_failures_detected = send_global_msg_to_check_state( idle_proc_list );
				if(num_failures_detected!=0) gaspi_printf("num_failures_detected %d\n", num_failures_detected);
			}
// 				comm_state = check_comm_health(status_processes, health_vec);	// TODO:	if any of the IDLE procs fails, then exclued it from the IDLE list of all the other procs too
// 				gaspi_printf("%dX", myrank_);
		}
		if(gm_ptr_failed_proc_list[0]!=WORKING){			// TODO: there can be check about if(myrank() != myrank_active		NOTE:// FAILURE REPORTED. meaning that health checker has provided it a list of failed processes
			while(myrank_ == myrank_active){				// To make sure that process has its repacement rank before he joins the main program.
				gaspi_printf("updatintg myrank_active %d \n", myrank_active );
				update_myrank_active(myrank_active);
				usleep(10000);
			}
			gaspi_printf("myrank: %d myrank_active %d , BROKEN reported i am going to save\n", myrank_ , myrank_active);
			break;
		}
		if(gm_ptr_status_processes[myrank_]==WORKFINISHED){		// WORKFINISHED REPORTED
			gaspi_printf("myrank: %d WorkFinished reported\n", myrank_);
			break;
		}
	}
	return 0;
}

int HC::send_global_msg_to_check_state( gaspi_rank_t * idle_proc_list){
	int num_sweeps_before_report_broken = 2;

	static gaspi_queue_id_t queue_id_counter = gm_ptr_queue_id_counter[0];
	gaspi_timeout_t HEALTH_CHECK_TIMEOUT_TIME = GASPI_BLOCK;
	gaspi_return_t retval;
	int comm_state=WORKING;
	int num_failed_procs = 0, num_failed_procs_idle=0;			// one variable for worker failures, one for idle failures.
	
	gaspi_rank_t * avoid_list_old = new gaspi_rank_t[numprocs_];
	memset(avoid_list_old, 0, numprocs_ * sizeof(gaspi_rank_t));
	copy_array_gaspirankt(gm_ptr_avoid_proc_list, avoid_list_old, numprocs_);			// src , dst 
	
	double ST_HC=0.0, ET_HC=0.0;
	double ST_hc=0.0, ET_hc=0.0, DT_hc=100000.0;
	double ST_temp=0.0, ET_temp=0.0, ST_temp_1=0.0, ET_temp_1=0.0;
	double ST_ping_all_procs=0.0, ET_ping_all_procs=0.0;
	
	int th_id = 0;
	get_walltime_(&ST_HC);
	
	omp_set_num_threads(1);			// There are maximum of 8 queues available. Can be increased to 16.
	while(DT_hc > 40.0){
		get_walltime_(&ST_hc);
		for(int i=0; i<numprocs_; ++i){
			if(gm_ptr_avoid_proc_list[i]!=BROKEN && gm_ptr_status_processes[i]==IDLE)
			{
				retval = gaspi_proc_ping(i, HEALTH_CHECK_TIMEOUT_TIME);
				if(retval==GASPI_ERROR){
					ASSERT_HC(gaspi_state_vec_get(gm_ptr_health_vec));
					print_health_vec();
					for(int j=0; j<numprocs_; ++j){								// adding the dead processes to gm_ptr_avoid_proc_list so that message for health test is not sent to them next time.
						if(gm_ptr_health_vec[j]==1 && gm_ptr_avoid_proc_list[j]!=BROKEN){
							gaspi_printf("Problem with node %d detected via gm_ptr_queue_id_counter[0] %d\n", j, gm_ptr_queue_id_counter[0]);
							gm_ptr_avoid_proc_list[j]=BROKEN;
							gm_ptr_status_processes[j]=BROKEN;
							num_failed_procs_idle++;
						}
					}
				}
			}
		}
		for(int i=0; i<numprocs_; ++i){
			if(gm_ptr_avoid_proc_list[i]!=BROKEN && gm_ptr_status_processes[i]==WORKING)
			{
				retval = gaspi_proc_ping(i, HEALTH_CHECK_TIMEOUT_TIME);
				if(retval==GASPI_ERROR)
				{
					ASSERT_HC(gaspi_state_vec_get(gm_ptr_health_vec));
					print_health_vec();
					for(int j=0; j<numprocs_; ++j){
						if(gm_ptr_health_vec[j]==1 && gm_ptr_avoid_proc_list[j]!=BROKEN){
							gaspi_printf("Problem with node %d detected via gm_ptr_queue_id_counter[0] %d\n", j, gm_ptr_queue_id_counter[0]);
							gm_ptr_avoid_proc_list[j]=BROKEN;
							num_failed_procs++;
						}
					}
					comm_state=BROKEN;
				}
			}
		}
		get_walltime_(&ET_hc);
		DT_hc = ET_hc - ST_hc;
// 		gaspi_printf("HC_time is %f\n", DT_hc);
	}
	if(num_failed_procs_idle !=0){										// NOTE: this is the case... if idle process encounter failure. Needs testing
		refresh_idle_proc_list( num_failed_procs_idle, idle_proc_list);
	}
	
// 	print_health_vec();
	if(comm_state == BROKEN){
		gaspi_printf("reporting failed processes\n" );
		make_failed_proc_list( avoid_list_old);
		make_rescue_proc_list( num_failed_procs, idle_proc_list, gm_ptr_rescue_proc_list);
		update_status_processes_array();
		report_failed_processes(num_failed_procs, idle_proc_list);						// it should only report the latest failures
		if(numprocs_idle_ > 0){
			memset(gm_ptr_failed_proc_list, 0, numprocs_ * sizeof(gaspi_rank_t));
		}
		comm_state = WORKING;
	}
	get_walltime_(&ET_HC);
// 	gaspi_printf("HC_time + failure propagation time is %f\n", ET_HC - ST_HC);
	return num_failed_procs;
}

int HC::refresh_idle_proc_list(int num_failed_procs_idle, gaspi_rank_t * idle_proc_list)
{
	//TODO: check if one of the idle proc is dead, then refresh the idle_proc_list, numprocs_idle_. and then return.
	int new_numprocs_idle = numprocs_idle_;

	for(int i=0; i<numprocs_idle_; ++i){
		if(gm_ptr_health_vec[idle_proc_list[i]]!=WORKING){
			new_numprocs_idle--;
			idle_proc_list[i]=-9;
		}
	}
	if(new_numprocs_idle != numprocs_idle_){
		gaspi_rank_t * new_idle_proc_list = new gaspi_rank_t[new_numprocs_idle];
		for(int j=0,i=0;j<numprocs_idle_;++j){
			if(idle_proc_list[j]!=WORKFINISHED){
				new_idle_proc_list[i++] = idle_proc_list[j];
			}
		}
		copy_array_gaspirankt(new_idle_proc_list, idle_proc_list, new_numprocs_idle);
		numprocs_idle_ = new_numprocs_idle;
	}
	gaspi_printf_array("new_idle_proc_list\n", idle_proc_list, numprocs_idle_);
	return 0;
}

int HC::make_failed_proc_list( gaspi_rank_t * avoid_list_old ){
	int num_failed_procs=0;
	for(int i=0;i<numprocs_;++i){
		if(gm_ptr_avoid_proc_list[i]==BROKEN && gm_ptr_avoid_proc_list[i]!=avoid_list_old[i]){		// the second condition checks if this 
			num_failed_procs++;
		}
	}
	gaspi_rank_t *failed_proc_nums = new gaspi_rank_t[num_failed_procs+1];	// first element carries the num_failures, rest of the elements carry the ids of failed procs
	init_array_gaspirankt(failed_proc_nums, num_failed_procs, -1);
	failed_proc_nums[0]=num_failed_procs;
	gaspi_printf("num_failed_procs %d\n", num_failed_procs);
	for(int i=1,j=1;i<numprocs_;++i){
		if(gm_ptr_avoid_proc_list[i]==BROKEN && gm_ptr_avoid_proc_list[i] != avoid_list_old[i]){
			failed_proc_nums[j]=(gaspi_rank_t)i;
			gaspi_printf("failed_proc_nums[%d]:%d\n", j , failed_proc_nums[j]);
			++j;
		}
	}
	// copy the list to global_mem
	copy_array_gaspirankt(failed_proc_nums, gm_ptr_failed_proc_list, num_failed_procs+1);
	return 0;
}

int HC::make_rescue_proc_list( int num_failed_procs, gaspi_rank_t *idle_proc_list, gaspi_rank_t * rescue_proc_list){
	rescue_proc_list[0]= (gaspi_rank_t) num_failed_procs;
	for(int i=0; i<num_failed_procs; ++i){
			rescue_proc_list[i+1] = (gaspi_rank_t) idle_proc_list[i];
	}
	gaspi_printf_array("The rescue_proc_list is \n", rescue_proc_list, num_failed_procs+1);
	return 0;
}

int HC::update_status_processes_array(){
	gaspi_printf_array("old gm_ptr_status_processes\n", gm_ptr_status_processes, numprocs_);
	
	for(int i=1;i<=gm_ptr_failed_proc_list[0];++i){
		gm_ptr_status_processes[gm_ptr_failed_proc_list[i]] = BROKEN;
	}
	
	int start_index_of_idle_proc=0;
	for(int i=0;i<numprocs_;++i){
		if(gm_ptr_status_processes[i]==IDLE){
			start_index_of_idle_proc = i;
			break;
		}
	}
	
	gaspi_printf("start_index_of_idle_proc %d\n", start_index_of_idle_proc);
	for(int i=start_index_of_idle_proc, j=1; i<start_index_of_idle_proc+gm_ptr_failed_proc_list[0]; ++i){
		gm_ptr_status_processes[i]=WORKING;
	}
	gaspi_printf_array("updated gm_ptr_status_processes\n", gm_ptr_status_processes, numprocs_);
	return 0;
}

int HC::report_failed_processes(int num_failed_procs, gaspi_rank_t * idle_proc_list){
	gaspi_timeout_t TIMEOUT_VAL = GASPI_BLOCK;
	gm_ptr_queue_id_counter[0]++;
	

	// message to working process
	for(gaspi_rank_t i=0; i<numprocs_; ++i){
		if( gm_ptr_avoid_proc_list[i] != BROKEN && gm_ptr_status_processes[i]==WORKING){
			gaspi_printf(" i m writing to %d val %d\n", i, gm_ptr_failed_proc_list[0]);
			ASSERT_HC(gaspi_write(gm_seg_rescue_proc_id, 		0, i, gm_seg_rescue_proc_id, 		0, sizeof(int)*(num_failed_procs+1),	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_write(gm_seg_status_processes_id, 	0, i, gm_seg_status_processes_id, 	0, sizeof(int)*numprocs_, 				gm_ptr_queue_id_counter[0], TIMEOUT_VAL));	// message the gm_ptr_status_processes to rescue processes.
			ASSERT_HC(gaspi_write(gm_seg_queue_id_counter_id, 	0, i, gm_seg_queue_id_counter_id, 	0, sizeof(int), 						gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_write(gm_seg_failed_proc_list_id, 	0, i, gm_seg_failed_proc_list_id, 	0, sizeof(int)*(num_failed_procs+1),	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			gaspi_wait(gm_ptr_queue_id_counter[0], TIMEOUT_VAL);					// NOTE: check if FT_gaspi_wait is required. maybe not, because we are checking BROKEN status in the if statement.
		}
	}
	
	for(int i=0; i<num_failed_procs;++i){
		gm_ptr_myrank_active_list[idle_proc_list[i]] = gm_ptr_myrank_active_list[gm_ptr_failed_proc_list[i+1]];				/// failed_proc_list[i+1]; 
	}
	
	// determine failed_proc_lists_active rank, and transmit that rank 
	
	gaspi_printf_array("gm_ptr_myrank_active_list", gm_ptr_myrank_active_list, numprocs_);
	
	
	// message to rescue process 
	for(int i=0; i<num_failed_procs; ++i){
			gaspi_rank_t proc_num = idle_proc_list[i];
			gaspi_printf(" i m writing to rescue procsess %d val %d\n", proc_num, gm_ptr_failed_proc_list[0]);
			ASSERT_HC(gaspi_write(gm_seg_rescue_proc_id, 		0, 								proc_num, gm_seg_rescue_proc_id, 		0, 								sizeof(int)*(num_failed_procs+1),	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_write(gm_seg_myrank_active_id, 	proc_num*sizeof(gaspi_rank_t), 	proc_num, gm_seg_myrank_active_id, 		proc_num*sizeof(gaspi_rank_t), 	sizeof(gaspi_rank_t), 				gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_write(gm_seg_status_processes_id, 	0, 								proc_num, gm_seg_status_processes_id, 	0,								sizeof(int)*numprocs_, 			gm_ptr_queue_id_counter[0], TIMEOUT_VAL));	// message the gm_ptr_status_processes to rescue processes.
			ASSERT_HC(gaspi_write(gm_seg_queue_id_counter_id, 	0, 								proc_num, gm_seg_queue_id_counter_id, 	0, 								sizeof(int), 						gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_write(gm_seg_failed_proc_list_id, 	0, 								proc_num, gm_seg_failed_proc_list_id, 	0, 								sizeof(int)*(num_failed_procs+1), 	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			gaspi_wait(gm_ptr_queue_id_counter[0], TIMEOUT_VAL);
	}
	
	int new_numprocs_idle = numprocs_idle_ - num_failed_procs;
	gaspi_printf("new_numprocs_idle: %d\n", new_numprocs_idle);
	gaspi_rank_t * new_idle_proc_list=new gaspi_rank_t[new_numprocs_idle];
// 	for(int i=0,j=0; i<= (numprocs_idle_ - num_failed_procs); ++i){
// 		new_idle_proc_list[j] = idle_proc_list[num_failed_procs+i];
// 	}
	
	for(int i=num_failed_procs, j=0; i<(numprocs_idle_); ++i, ++j){
		new_idle_proc_list[j] = idle_proc_list[i];
	}
	
	gaspi_printf_array("The new_idle_proc_list is \n", new_idle_proc_list, new_numprocs_idle);
	copy_array_gaspirankt(new_idle_proc_list, idle_proc_list, new_numprocs_idle);		// src, dst
	numprocs_idle_ = new_numprocs_idle;

	
	gaspi_printf_array("The list of failed procs is \n", gm_ptr_failed_proc_list, gm_ptr_failed_proc_list[0]+1);
	gaspi_printf_array("The new_idle_proc_list is \n", idle_proc_list, numprocs_idle_);
	
	return 0;
}

int HC::refresh_numprocs_working_and_idle(){
	int refresh_numprocs_working_and_idle_=0;
	for(int i=0;i<numprocs_;++i){
		if(gm_ptr_status_processes[i] == WORKING || gm_ptr_status_processes[i] == IDLE)
			refresh_numprocs_working_and_idle_++;
	}
	gaspi_printf("refresh_numprocs_working_and_idle: %d\n", refresh_numprocs_working_and_idle_);
	return refresh_numprocs_working_and_idle_;
}

int HC::update_myrank_active( gaspi_rank_t & myrank_active){
	myrank_active = gm_ptr_myrank_active_list[myrank_];
	gaspi_printf("=== MYRANK: %d, MY_ACTIVE_RANK: %d \n", myrank_, myrank_active);
	return 0;
}

bool HC::am_i_rescue_process (){
	bool am_i_rescue_proc_=false;
	for(gaspi_rank_t i=1; i<=gm_ptr_rescue_proc_list[0]; ++i){
		if(myrank_ == gm_ptr_rescue_proc_list[i]){
			am_i_rescue_proc_ = true;
		}
	}
	return am_i_rescue_proc_;
}

int HC::recover_comm( gaspi_group_t * COMM_MAIN, const gaspi_rank_t numprocs_working, gaspi_rank_t * comm_main_ranks_ordered)
{
	
	int static first_recovery_run = true;
	if(first_recovery_run == true){
		previous_comm_main_rank_working_order = new gaspi_rank_t[numprocs_working];
		ASSERT_HC(gaspi_group_ranks (*(COMM_MAIN-1), previous_comm_main_rank_working_order));	// calculating the ranks of precious COMM_MAIN;
		first_recovery_run = false;
	}
	double ST_recovery = 0.0, ET_recovery = 0.0;
	gaspi_return_t ret;
	
	gaspi_printf("BROKEN reported \n");
	gaspi_printf_array("======== gm_ptr_status_processes ======== \n", gm_ptr_status_processes, numprocs_);
	gaspi_printf_array("======== failed gm_ptr_failed_proc_list ======== \n", gm_ptr_failed_proc_list, gm_ptr_failed_proc_list[0]+1);
	gaspi_printf_array("======== gm_ptr_rescue_proc_list ======== \n", gm_ptr_rescue_proc_list, gm_ptr_rescue_proc_list[0]+1);
	int num_failures = gm_ptr_failed_proc_list[0];		// num failures in current iteration check
	
	
	gaspi_printf("before gaspi_group_delete(COMM_MAIN)\n");
// 	ASSERT_HC(gaspi_group_delete(*(COMM_MAIN-1)));			// BUG: this should be used to remove the older COMM_MAIN but seems to be a bug in gaspi side.
// 	gaspi_printf("after gaspi_group_delete(COMM_MAIN)\n");
	bool am_i_rescue_proc = am_i_rescue_process();

// 	for(int i=1; i<=gm_ptr_failed_proc_list[0] ; ++i){
// 		ret = gaspi_proc_kill(gm_ptr_failed_proc_list[i], GASPI_BLOCK);
// 		PRINT_RETVAL(ret);
// 	}
	
	gaspi_rank_t * comm_main_new_ranks = new gaspi_rank_t[numprocs_working];
	init_array_gaspirankt(comm_main_new_ranks, numprocs_working, 0);
	this->define_sub_comm( WORKING, numprocs_working, COMM_MAIN, comm_main_new_ranks);	
	gaspi_barrier(*COMM_MAIN, GASPI_BLOCK);
	init_array_gaspirankt(comm_main_ranks_ordered, numprocs_working, 0);
	copy_array_gaspirankt(previous_comm_main_rank_working_order, comm_main_ranks_ordered , numprocs_working);
	
	gaspi_barrier(*COMM_MAIN, GASPI_BLOCK);
	refresh_comm_main_ranks_working_order( COMM_MAIN, previous_comm_main_rank_working_order, comm_main_ranks_ordered, numprocs_working);
	
	copy_array_gaspirankt(comm_main_ranks_ordered, previous_comm_main_rank_working_order, numprocs_working);
	
	gaspi_printf("recovery routine done1 \n");
 	gaspi_barrier(*COMM_MAIN, GASPI_BLOCK);
	gaspi_printf("recovery routine done2\n");

	return 0;
}

int HC::signal_all_processes(int signal){
// 	gaspi_printf_array("==== STATUS PROCESSES ====\n", gm_ptr_status_processes, numprocs_);
	gaspi_timeout_t TIMEOUT_VAL = GASPI_BLOCK;
// 	gaspi_printf("signal_idle_process with gm_ptr_queue_id_counter[0]: %d\n", gm_ptr_queue_id_counter[0]);
	
	for(int i=0; i<numprocs_; ++i){
		if(gm_ptr_status_processes[i]==IDLE || gm_ptr_status_processes[i]==WORKING){
			gm_ptr_status_processes[i] = signal;
		}
	}
	ASSERT_HC(gaspi_wait(gm_ptr_queue_id_counter[0],GASPI_BLOCK));
// 	gaspi_printf_array("==== STATUS PROCESSES ====\n", gm_ptr_status_processes, numprocs_);
	for(int i=1; i<numprocs_; ++i){
		if(gm_ptr_status_processes[i]==signal){
// 			gaspi_printf ("writing to proc %d: \n", i);
			ASSERT_HC(gaspi_write(gm_seg_status_processes_id, 0, (gaspi_rank_t) i, gm_seg_status_processes_id, 0, (gaspi_rank_t)(sizeof(int)*numprocs_),	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
			ASSERT_HC(gaspi_wait(gm_ptr_queue_id_counter[0],GASPI_BLOCK));
		}
	}
	return 0;
}


int HC::define_sub_comm( int flag, gaspi_rank_t numprocs_COMM, gaspi_group_t * COMM, gaspi_rank_t * comm_ranks)			// creates a sub-comm based on the flag.
{
	// ===== START GASPI group creation =====

	for(int i=0; i<numprocs_COMM; ++i){
		comm_ranks[i] = (gaspi_rank_t) 0;
	}

	gaspi_number_t gsize = 0;
	if(gm_ptr_status_processes[myrank_]==flag){
		for(gaspi_number_t i=0; i<numprocs_; i++)
		{
			if(gm_ptr_status_processes[i]==flag){
				ASSERT_HC(gaspi_group_add(*COMM, i));
				ASSERT_HC(gaspi_group_size(*COMM, &gsize));
				if(gsize==numprocs_COMM)
					break;
			}
		}
		ASSERT_HC(gaspi_group_ranks (*COMM, comm_ranks));
		
		if(flag == WORKING)
			gaspi_printf_array("printing COMM_MAIN: \n", comm_ranks, numprocs_COMM);
		if(flag == IDLE)
			gaspi_printf_array("printing COMM_IDLE: \n", comm_ranks, numprocs_COMM);
		
		ASSERT_HC(gaspi_group_commit (*COMM, GASPI_BLOCK));
	}
	return 0;
	// ===== END GASPI group creation =====
}

int HC::print_health_vec(){
	gaspi_printf("checking and printing state\n");
	for(int i=0;i<numprocs_; ++i){
		gaspi_printf("%d_health is: %d \n", myrank_, gm_ptr_health_vec[i]);
	}
	return 0;
}


template <class gm_ptr_typ> int HC::allocate_gaspi_global_mem(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gm_ptr_typ ** global_array, gaspi_group_t gaspi_group){
// 	*global_array = (gm_ptr_typ *) malloc(gm_size);				// NOTE: maybe this allocation is unnecessary as gaspi_segment_create will also allocate mem.
	if(GASPI_SUCCESS!=gaspi_segment_create(gm_seg_id, gm_size, gaspi_group, GASPI_BLOCK, GASPI_MEM_INITIALIZED))
	{
		gaspi_printf("ERROR: call segment_create for gm_seg_id=%d is not a success...EXITING\n", gm_seg_id);
		exit (-1);
	}
	gaspi_pointer_t gm_ptr_;
	if (gaspi_segment_ptr (gm_seg_id, &gm_ptr_) != GASPI_SUCCESS)
	{
		gaspi_printf ("ERROR: call gaspi_segment_ptr for gm_seg_id=%d is not a success...EXITING\n", gm_seg_id);
		exit (-1);
	}
	*global_array = (gm_ptr_typ *) gm_ptr_;
	memset(*global_array, 0, gm_size);
	return 0;
}

int HC::allocate_gaspi_global_mem_int(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, int ** global_array, gaspi_group_t gaspi_group){
	return allocate_gaspi_global_mem<int>(gm_seg_id, gm_size, global_array, gaspi_group);
}

int HC::allocate_gaspi_global_mem_rankt(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gaspi_rank_t ** global_array, gaspi_group_t gaspi_group){
	return allocate_gaspi_global_mem<gaspi_rank_t>(gm_seg_id, gm_size, global_array, gaspi_group);
}

int HC::allocate_gaspi_global_mem_statevectort(gaspi_segment_id_t gm_seg_id, gaspi_size_t gm_size, gaspi_state_vector_t * state_vec, gaspi_group_t gaspi_group){
	if(GASPI_SUCCESS!=gaspi_segment_create(gm_seg_id, gm_size, gaspi_group, GASPI_BLOCK, GASPI_MEM_INITIALIZED))
	{
		gaspi_printf("ERROR: call segment_create for gm_seg_id=%d is not a success...EXITING\n", gm_seg_id);
		exit (-1);
	}
	gaspi_pointer_t gm_ptr_;
	if (gaspi_segment_ptr (gm_seg_id, &gm_ptr_) != GASPI_SUCCESS)
	{
		gaspi_printf ("ERROR: call gaspi_segment_ptr for gm_seg_id=%d is not a success...EXITING\n", gm_seg_id);
		exit (-1);
	}
	*state_vec = (gaspi_state_vector_t ) gm_ptr_;
 	ASSERT_HC(gaspi_state_vec_get(*state_vec));
	return 0;
}



int HC::gaspi_printf_array(char * print_str, int * array_ptr, int num_elem){
	gaspi_printf("%s", print_str);
	for(int i=0;i<num_elem ;++i){
		gaspi_printf("%d\n", array_ptr[i]);
	}
	return 0;
}
int HC::gaspi_printf_array(char * print_str, gaspi_rank_t * array_ptr, int num_elem){
	gaspi_printf("%s", print_str);
	for(int i=0;i<num_elem ;++i){
		gaspi_printf("%hi\n", array_ptr[i]);
	}
	return 0;
}
int HC::gaspi_printf_array(char * print_str, double * array_ptr, int num_elem){
	gaspi_printf("%s", print_str);
	for(int i=0;i<num_elem ;++i){
		gaspi_printf("%4.9f\n", array_ptr[i]);
	}
	return 0;
}


template <class init_array_typ> int HC::init_array(init_array_typ * to_init_array, int num_elem, init_array_typ val){
	for(int i=0;i<num_elem ;++i){
		to_init_array[i]=val;
	}
	return 0;
}
int HC::init_array_dbl(double * to_init_array, int num_elem, double val){
	return init_array(to_init_array, num_elem, val);
}
int HC::init_array_int(int * to_init_array, int num_elem, int val){
	return init_array(to_init_array, num_elem, val);
}
int HC::init_array_gaspirankt( gaspi_rank_t * to_init_array, int num_elem, gaspi_rank_t val){
	return init_array(to_init_array, num_elem, val);
}

template <class copy_array_typ> int HC::copy_array(copy_array_typ * src, copy_array_typ * dst, int array_len){
	for(int i=0; i<array_len; ++i){
		dst[i] = src[i];
	}
	return 0;
}
int HC::copy_array_int(int * src, int * dst, int array_len){
	return copy_array<int>(src, dst, array_len);
}
int HC::copy_array_gaspirankt(gaspi_rank_t * src, gaspi_rank_t * dst, int array_len){
	return copy_array<gaspi_rank_t>(src, dst, array_len);
}

void success_or_exit_HC ( const char* file, const int line, const int ec)
{
  if (ec != GASPI_SUCCESS)
    {
      gaspi_printf ("Assertion failed in %s[%i]:%d\n", file, line, ec);
      
      exit (EXIT_FAILURE);
    }
}

int HC::refresh_comm_main_ranks_working_order(gaspi_group_t *COMM_MAIN, gaspi_rank_t * previous_comm_main_rank_working_order, gaspi_rank_t * comm_main_ranks_ordered, gaspi_rank_t numprocs_working){
	// proc-0 caculates puts the ranks in the proper order and then sends to everyone else

	int * comm_main_ranks_ordered_temp = new int[numprocs_working];
	int * comm_main_ranks_ordered_temp1 = new int[numprocs_working];
	init_array_int(comm_main_ranks_ordered_temp, numprocs_working, 0);
	init_array_int(comm_main_ranks_ordered_temp1, numprocs_working, 0);
	
	
	
	
	if(myrank_==0){
		for(int i=0; i< numprocs_working; ++i){
			comm_main_ranks_ordered_temp1[i] = (int) comm_main_ranks_ordered[i];
		}
// 		copy_array_gaspirankt(comm_main_ranks_ordered, comm_main_ranks_ordered_temp, numprocs_working);
		if(HClib_verbose >= 2) gaspi_printf_array("previous_comm_main_rank_working_order \n", previous_comm_main_rank_working_order, numprocs_working);
		for(gaspi_rank_t i=0; i!=gm_ptr_failed_proc_list[0]; ++i){
			int failed_proc_num = gm_ptr_failed_proc_list[i+1];
				for(gaspi_rank_t j=0; j!=numprocs_working; ++j){
					if(failed_proc_num == previous_comm_main_rank_working_order[j])
						comm_main_ranks_ordered_temp1[j] = gm_ptr_rescue_proc_list[i+1];
			}
		}
	}
	
	init_array_gaspirankt(comm_main_ranks_ordered, numprocs_working, 0);
	
	
	gaspi_barrier(*COMM_MAIN, GASPI_BLOCK);
	gaspi_allreduce (comm_main_ranks_ordered_temp1, comm_main_ranks_ordered_temp, numprocs_working, GASPI_OP_SUM, GASPI_TYPE_INT, *COMM_MAIN, GASPI_BLOCK);
	gaspi_barrier(*COMM_MAIN, GASPI_BLOCK);
	
	for(int i=0; i< numprocs_working; ++i){
		comm_main_ranks_ordered[i] = (gaspi_rank_t) comm_main_ranks_ordered_temp[i];
	}
	
	if(HClib_verbose >= 2) gaspi_printf_array("comm_main_ranks_ordered \n", comm_main_ranks_ordered, numprocs_working);
	return 0;
}

int HC::reset_gm_ptr_failed_proc_list(){
	memset(gm_ptr_failed_proc_list, 0, (numprocs_*sizeof(gaspi_rank_t)));
	return 0;
}


int init_myrank_active(HC * myHC){
	gaspi_rank_t numprocs_;
	gaspi_proc_num(&numprocs_);
	for(gaspi_rank_t i =0; i< numprocs_; ++i ){
		myHC->gm_ptr_myrank_active_list[i] =  (gaspi_rank_t)i;
	}
	return 0;
}


int HC::get_status(){
	return gm_ptr_status_processes[myrank_];
}	

// int signal_idle_processes(int signal, gaspi_segment_id_t gm_seg_status_processes_id, int * gm_ptr_status_processes, gaspi_segment_id_t gm_seg_queue_id_counter_id, int * gm_ptr_queue_id_counter){
// 	gaspi_printf_array("==== STATUS PROCESSES ====\n", gm_ptr_status_processes, numprocs_);
// 	gaspi_timeout_t TIMEOUT_VAL = GASPI_BLOCK;
// 	gaspi_printf("signal_idle_process with gm_ptr_queue_id_counter[0]: %d\n", gm_ptr_queue_id_counter[0]);
// 	
// 	for(int i=0; i<numprocs_; ++i){
// 		if(gm_ptr_status_processes[i]==IDLE){
// 			gm_ptr_status_processes[i] = signal;
// 		}
// 	}
// 	ASSERT_HC(gaspi_wait(gm_ptr_queue_id_counter[0],GASPI_BLOCK));
// 	gaspi_printf_array("==== STATUS PROCESSES ====\n", gm_ptr_status_processes, numprocs_);
// 	for(int i=1; i<numprocs_; ++i){
// 		if(gm_ptr_status_processes[i]==signal){
// 			gaspi_printf ("writing to proc %d: \n", i);
// 			ASSERT_HC(gaspi_write(gm_seg_status_processes_id, 0, (gaspi_rank_t) i, gm_seg_status_processes_id, 0, (gaspi_rank_t)(sizeof(int)*numprocs_),	gm_ptr_queue_id_counter[0], TIMEOUT_VAL));
// 			ASSERT_HC(gaspi_wait(gm_ptr_queue_id_counter[0],GASPI_BLOCK));
// 		}
// 	}
// 	return 0;
// }



